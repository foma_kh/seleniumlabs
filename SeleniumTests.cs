﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumLab
{
    [TestFixture]
    public class SeleniumTests
    {
        IWebDriver driver = new ChromeDriver(@"C:\ChromeDriver");
        bool isElementDisplayed;
        [Test]
        public void WizzAirTest()
        {
            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
            driver.Navigate().GoToUrl("http://wizzair.com");

            IWebElement departureInput = wait.Until(driver => driver.FindElement(By.Id("search-departure-station")));
            departureInput.Clear();
            String departureCity = "Kiev";
            String arrivalCity = "Copenhagen";
            departureInput.SendKeys(departureCity);
            IWebElement confirmDeparture = driver.FindElement(By.ClassName("locations-container__location"));
            confirmDeparture.Click();
            IWebElement arrivalInput = driver.FindElement(By.Id("search-arrival-station"));
            arrivalInput.SendKeys(arrivalCity);
            IWebElement confirmArrival = driver.FindElement(By.ClassName("locations-container__location"));
            confirmArrival.Click();
            String enteredDeparture = departureInput.Text;
            String enteredArrival = arrivalInput.Text;

            IWebElement selectedDate = driver.FindElement(By.Id("search-departure-date"));
            String sSelectedDate = selectedDate.Text;//save the date in format '11 May 2018'

            IWebElement searchButton = driver.FindElement(By.XPath("//button[@tabindex='2']"));
            searchButton.Click();

            try
            {
                driver.SwitchTo().Window(driver.WindowHandles[1]); //switch to new window
            }
            catch (ArgumentOutOfRangeException)
            {
                driver.SwitchTo().Window(driver.WindowHandles[0]); //if new window is not diplayed - dont switch 
            }
            
            IWebElement departureDay = wait.Until(driver => driver.FindElement(By.XPath("//*[contains(@class,'4 date')]")));
            String sDepartureDay = departureDay.Text; //save the date in format  'Fri, 11 May 2018'
            Assert.That(sDepartureDay, Does.Contain(sSelectedDate), "The departureDay shoud be equal to selected Day");

            IWebElement flightRoute = driver.FindElement(By.XPath("//*[@id='fare-selector-outbound']//address"));
            String sFlightRoute = flightRoute.Text;
            StringAssert.StartsWith(enteredDeparture, sFlightRoute, "The departure should be equal entered city");//Checking the departure City
            StringAssert.EndsWith(enteredArrival, sFlightRoute, "The arrival should be equal entered city");//Checking the arrival City

            IWebElement highlightedDate = driver.FindElement(By.XPath("//li[contains(@class,'--selected')]//time"));
            String sHighlightedDate = highlightedDate.Text;
            DateTime date1 = DateTime.Parse(sHighlightedDate);
            DateTime date2 = DateTime.Parse(sDepartureDay);
            Assert.AreEqual(date1, date2, "The Highlighted Date should be equal to departure date"); //checking the correct date is selected

            IList<IWebElement> threePrices = driver.FindElements(By.XPath("//div[@class='rf-fare']"));
            Assert.That(threePrices.Count == 3, "Only 3 prices should be displayed");//3 options with different prices are displayed including prices

            try
            {
                isElementDisplayed = driver.FindElement(By.Id("fare-selector-return")).Displayed;
            }
            catch (NoSuchElementException)
            {
                isElementDisplayed = false;
            }
            Assert.IsFalse(condition: isElementDisplayed, message: "Error! Return flights are displayed");//check the return flight is not displayed

            IWebElement middleButton = driver.FindElement(By.XPath("(//div[@class='rf-fare__price'])[3]"));
            middleButton.Click();
            IWebElement continueButton = driver.FindElement(By.Id("flight-select-continue-btn"));
            continueButton.Submit();
            IWebElement firstNameInput = wait.Until(driver => driver.FindElement(By.Id("passenger-first-name-0")));
            firstNameInput.SendKeys("Ivan");
            IWebElement lastNameInput = wait.Until(driver => driver.FindElement(By.Id("passenger-last-name-0")));
            lastNameInput.SendKeys("Ivanov");
            IWebElement maleButton = driver.FindElement(By.CssSelector("[data-test=passenger-gender-0-male]"));
            maleButton.Click();
            IWebElement passengerContinueButton = driver.FindElement(By.Id("passengers-continue-btn"));
            passengerContinueButton.Submit();
            IWebElement signInPopup = wait.Until(dr => dr.FindElement(By.Id("login-modal")));
            Assert.IsTrue(condition: signInPopup.Displayed, message: "Error! SignInPopup is not displayed");
            driver.Quit();
        }
    }
}
